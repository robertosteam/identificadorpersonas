/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebadirectorio;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ListaDePersonas")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataImport {
    
    @XmlElement(name = "Persona")
    public static ArrayList <Persona> listaPersonas = new ArrayList();
    
    public static void DataImport(){
        
         String nombreFichero = "agenda.csv";
        //Declarar una variable BufferedReader
        BufferedReader br = null;
        
        // formato de la fecha 
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yy");
        try {
           //Crear un objeto BufferedReader al que se le pasa 
           //   un objeto FileReader con el nombre del fichero
           br = new BufferedReader(new FileReader(nombreFichero));
           //Leer la primera línea, guardando en un String
           String texto = br.readLine();
           
           //Repetir mientras no se llegue al final del fichero
           while(texto != null) {
               String[] data = texto.split(";");
               Date fecha = null;
               
            try {
                fecha = formatoDelTexto.parse(data[9]);
                   
                } catch (ParseException ex) {
                   System.out.println("Error al convertir el texto a fecha");
                   System.out.println(ex.getMessage());
               
               } 
               
               
               Persona persona = new Persona (data[0], data[1], data[2], data[3],data[4], data[5], data[6], data[7], data[8], fecha);
               listaPersonas.add(persona);
               System.out.println(persona.name);
               //Leer la siguiente línea
               texto = br.readLine();
           }
        }
        //cambiar los systemprin por logger
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
            System.out.println(e.toString());
        }
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
        
//        mostrarAlumnos();
    
    }

    private static void mostrarAlumnos() {
        for (int i = 0; i < listaPersonas.size(); i++) {
            Persona p = listaPersonas.get(i);
            System.out.println(p.getName() + " " + p.getCodigoPostal());
        }
    }
    
    
}
