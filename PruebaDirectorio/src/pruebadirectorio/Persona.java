/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebadirectorio;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Roberto
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Persona {

    @XmlElement
    public String name;
    @XmlElement
    public String surnames;
    @XmlElement
    public String phone;
    @XmlElement
    public String sexo;
    @XmlElement
    public String direccion;
    @XmlElement
    public String codigoPostal;
    @XmlElement
    public String dni;
    @XmlElement
    public String localidad;
    @XmlElement
    public String telefonoFijo;
    @XmlElement
    public Date fechaNacimiento;

    /**
     * Constructor con todos los datos vacíos
     */
    public Persona() {
        this.name = "";
        this.surnames = "";
        this.phone = "";
        this.sexo = "";
        this.direccion = "";
        this.codigoPostal = "";
        this.dni = "";
        this.localidad = "";
        this.telefonoFijo = "";
        this.fechaNacimiento = null;
    }

    /**
     * Constructor con todos los datos
     */
    public Persona(String name, String surname, String phone, String sexo, String direccion, String codigoPostal,
            String dni, String localidad, String telefonoFijo, Date fechaNacimiento) {
        this.name = name;
        this.surnames = surname;
        this.phone = phone;
        this.sexo = sexo;
        this.direccion = direccion;
        this.codigoPostal = codigoPostal;
        this.dni = dni;
        this.localidad = localidad;
        this.telefonoFijo = telefonoFijo;
        this.fechaNacimiento = fechaNacimiento;
    }

    //Get y Set de Nombre.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Get y Set de Apellido.
    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    //Get y Set de Telefono.
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    //Get y Set de Sexo.    
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    //Get y Set de Direccion.
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    //Get y Set de Codigo Postal.
    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    // Get y Set de dni
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    //Get y Set de Localidad.    
    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    //Get y Set de Telefono Fijo.    
    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    //Get y Set de Fecha Nacimiento.    
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

}
