/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebadirectorio;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Roberto
 */
public class MainUnmarshallURL extends Application {
    
    public static StackPane root;
    private TableView<Persona> tabla;
    private  ObservableList<Persona> datos;
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        
        // iniciar StackPane y TableView
        root = new StackPane();
        tabla = new TableView<>();
        
        // carga de datos en ObservableList desde archivo XML
        cargarDatosDesdeXML();
        datos = FXCollections.observableArrayList(DataImport.listaPersonas);
        mostrarAlumnos(DataImport.listaPersonas);
        
        Scene scene = new Scene(root);
        stage.setTitle("Identificación de personas");
        //stage.setWidth(450);
        //stage.setHeight(500);
 
        final Label label = new Label("Listado de alumnos");
        label.setFont(new Font("Arial", 20));
 
        tabla.setEditable(false);
 
        TableColumn firstNameCol = new TableColumn("Nombre");
        firstNameCol.setMinWidth(200);
        firstNameCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("name"));
 
        TableColumn lastNameCol = new TableColumn("Apellidos");
        lastNameCol.setMinWidth(200);
        lastNameCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("surnames"));
        
        TableColumn fechaNacimientoCol = new TableColumn("Fecha de nacimiento");
        fechaNacimientoCol.setMinWidth(200);
        fechaNacimientoCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("fechaNacimiento"));
 
        setDateFormatColumn(fechaNacimientoCol, "dd/MM/yyyy");
        
        tabla.setItems(datos);
        tabla.getColumns().addAll(firstNameCol, lastNameCol, fechaNacimientoCol);
        Button btnSuprimir = new Button("Suprimir");
        btnSuprimir.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                System.out.println("SUPRIMIR");
                Persona p = tabla.getSelectionModel().getSelectedItem();
                if (p != null) {
                    System.out.println(p.getName());
                    DataImport.listaPersonas.remove(p);
                    datos.remove(p);
                    System.out.println("Elemento borrado");
                } else {
                    System.out.println("Ningún elemento seleccionado");
                }
            }
        });
        Button btnNuevo = new Button("Nuevo");
        btnNuevo.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                System.out.println("NUEVO");
                Persona nuevaPersona = new Persona();
                tabla.getItems().add(nuevaPersona);
                tabla.getSelectionModel().select(nuevaPersona);
                mostrarPantallaDetalle(true);
            }
        });
        Button btnEditar = new Button("Editar");
        btnEditar.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                System.out.println("EDITAR");
                mostrarPantallaDetalle(false);
            }
        });
        
        // agrupar los botones en horizontal
        final HBox hbox = new HBox();
        hbox.getChildren().addAll(btnSuprimir, btnNuevo, btnEditar);
        hbox.setSpacing(15);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        
        // agrupar los elementos en vertical
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.getChildren().addAll(label, tabla, hbox);
 
        root.getChildren().addAll(vbox);
 
        stage.setScene(scene);
        stage.show();    
    }

   private static void mostrarAlumnos(ArrayList<Persona> listaPersonas) {
        for (Persona p : listaPersonas) {
            System.out.println(p.getName() + " " + p.getCodigoPostal());
        }
    }
    
   
   private void setDateFormatColumn(TableColumn dateColumn, String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        dateColumn.setCellFactory(myDateTableCell -> {
            return new TableCell<Object, Date>() {
                @Override
                protected void updateItem(Date date, boolean dateIsEmpty) {
                    super.updateItem(date, dateIsEmpty);
                    if (date == null || dateIsEmpty) {
                        setText(null);
                    } else {
                        setText(simpleDateFormat.format(date));
                    }
                }
            };
        });
    }

    /**
     * Muestra la pantalla con los detalles de la persona seleccionada.
     * @param isPersonaNueva true si se añade una persona nueva, false si se edita una persona existente
     */
    private void mostrarPantallaDetalle(boolean isPersonaNueva) {
        Persona p = tabla.getSelectionModel().getSelectedItem();
        if (p != null) {
            Parent parent = null;
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
                parent = loader.load();
                FXMLDocumentController pantallaDetalleControlador = loader.getController();
                // pasar el TableView para que la pantalla detalle pueda acceder a su contenido
                pantallaDetalleControlador.setTableView(tabla);
                // rellenar los detalles con los datos del objeto actual
                pantallaDetalleControlador.moreDetails();
                // establecer si es una persona nueva o se está editando una existente
                pantallaDetalleControlador.isPersonaNueva(isPersonaNueva);
            } catch (IOException ex) {
                Logger.getLogger(MainUnmarshallURL.class.getName()).log(Level.SEVERE, null, ex);
                Platform.exit();
            }
            root.getChildren().addAll(parent);
            System.out.println("Nueva Persona");
        } else {
            System.out.println("Ningún elemento seleccionado");
        }
    }

    private void cargarDatosDesdeXML() {
        try {
            // Crear objeto JAXB para interpretar objetos desde XML
            JAXBContext jaxbContext = JAXBContext.newInstance(DataImport.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            // Generar lista de objetos desde archivo XML descargado de URL
            
            URL url = new URL("http://robertogonzalez.esy.es/listaDePersonas.xml");
            InputStream is = url.openStream();
            DataImport personas = (DataImport) jaxbUnmarshaller.unmarshal(is);

            // Mostrar el contenido de la lista
            System.out.println("Lista de personas cargadas desde archivo XML");
            for (Persona p : personas.listaPersonas) {
                System.out.println("Nombre: " + p.getName());
                System.out.println("Apellidos: " + p.getSurnames());
                System.out.println("Sexo: " + p.getSexo());
                System.out.println("DNI: " + p.getDni());
                System.out.println("Direccion: " + p.getDireccion());
                System.out.println("Código Postal: " + p.getCodigoPostal());
                System.out.println("Localidad: " + p.getLocalidad());
                System.out.println("Teléfono fijo: " + p.getTelefonoFijo());
                System.out.println("Teléfono Móvil: " + p.getPhone());
                System.out.println("Fecha de nacimiento: " + p.getFechaNacimiento());
                System.out.println();
            }
        } catch (Exception ex) {
            Logger.getLogger(MainUnmarshallURL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
