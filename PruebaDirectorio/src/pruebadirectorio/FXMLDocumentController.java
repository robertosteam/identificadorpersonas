/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebadirectorio;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Roberto
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextField textFieldNombre;
    @FXML
    private TextField textFieldSexo;
    @FXML
    private TextField textFieldDni;
    @FXML
    private TextField textFieldApellidos;
    @FXML
    private TextField textFieldDireccion;
    @FXML
    private TextField textFieldCP;
    @FXML
    private TextField textFieldLocalidad;
    @FXML
    private TextField textFieldTfnoFijo;
    @FXML
    private TextField textFieldTfnoMovil;
    @FXML
    private DatePicker textFieldFecha;

    private Persona persona;
    private TableView<Persona> tabla;
    private boolean isPersonaNueva;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         
    }   
    
    public void setTableView(TableView<Persona> tabla){
        this.tabla = tabla;
    
    }
    //Metodo para mostrar los detalles de la persona selccionada
    public void moreDetails(){
        //guardar en una variable la persona seleccionada
        persona = tabla.getSelectionModel().getSelectedItem();
        //rellenar los textefield
        textFieldNombre.setText(persona.getName());
        textFieldApellidos.setText(persona.getSurnames());
        textFieldSexo.setText(persona.getSexo());
        textFieldDireccion.setText(persona.getDireccion());
        textFieldDireccion.setText(persona.getDni());
        textFieldCP.setText(persona.getCodigoPostal());
        textFieldLocalidad.setText(persona.getLocalidad());
        textFieldTfnoFijo.setText(persona.getTelefonoFijo());
        textFieldTfnoMovil.setText(persona.getPhone());
        UtilJavaFx.setDateInDatePicker(textFieldFecha, persona.getFechaNacimiento());
    }

    @FXML
    private void onMouseClickedButtonGuardar(MouseEvent event) {
        // actualizar propiedades del objeto
        persona.setName(textFieldNombre.getText());
        persona.setSurnames(textFieldApellidos.getText());
        persona.setSexo(textFieldSexo.getText());
        persona.setDni(textFieldDni.getText());
        persona.setDireccion(textFieldDireccion.getText());
        persona.setCodigoPostal(textFieldCP.getText());
        persona.setLocalidad(textFieldLocalidad.getText());
        persona.setTelefonoFijo(textFieldTfnoFijo.getText());
        persona.setPhone(textFieldTfnoMovil.getText());
        persona.setFechaNacimiento(UtilJavaFx.getDateFromDatePicket(textFieldFecha));
        

        // actualizar persona en la tabla, asignando el mismo objeto en la posición actual de la lista
        int i = tabla.getSelectionModel().getSelectedIndex();
        tabla.getItems().set(i, persona);
        
         cerrarPantallaDetalle();
    }
    
    private void cerrarPantallaDetalle() {
        int lastScreensNumber = pruebaMain.root.getChildren().size() - 1;
        pruebaMain.root.getChildren().remove(lastScreensNumber);
    }
    
    @FXML
    private void onMouseClickedButtonCancelar(MouseEvent event) {
        // si se cancela la creación de una persona nueva eliminarla
        if (isPersonaNueva) {
            Persona p = tabla.getSelectionModel().getSelectedItem();
            tabla.getItems().remove(p);
        }
        cerrarPantallaDetalle();
    }

    void isPersonaNueva(boolean isPersonaNueva) {
        this.isPersonaNueva = isPersonaNueva;
    }

}
