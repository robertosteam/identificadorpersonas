/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buscaminasgold;

import java.util.Random;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


// Creacion de variables que vamos a usar 
public class GestorInfo {
    private static final Logger REGISTRO = Logger.getLogger("com.buscaminasgold.GestorInfo");
    private BM_Celda[][] tablero;
    private int horizontal;
    private int vertical;
    private int minas;
 
    /**
     * Metodo contructor de la clase gestorInfo
     * @param horizontal Tamaño horizontal del tablero
     * @param vertical Tamaño vertical del tablero
     * @param totalMinas Cantidad total de minas creadas en tablero
     */
    public GestorInfo(int horizontal, int vertical, int totalMinas) {
        REGISTRO.addHandler(new ConsoleHandler());
        
        //Creacion del tablero
        tablero = new BM_Celda[horizontal][vertical];
        this.horizontal = horizontal;
        this.vertical = vertical;
        this.minas = totalMinas;
        
        //Control para que las minas no sean mas numerosas que las casillas
        if (totalMinas > (horizontal * vertical) ) {
            totalMinas = horizontal * vertical;
            REGISTRO.warning("El número de minas es mayor que el número de celdas. Se fija el número de minas a: " + totalMinas);
        }
        
        //Iniciacion de la matriz (Array)
        for (int i = 0; i < horizontal; i++) {
            for (int j = 0; j < vertical; j++) {
                tablero[i][j] = new BM_Celda();
            }
        }
        
        //Colocar las minas al azar
        Random azar = new Random();
        int numeroMinasColocadas = 0;
        while (numeroMinasColocadas < totalMinas) {            
            int i = azar.nextInt(horizontal);
            int j = azar.nextInt(vertical);
            if (!tablero[i][j].getMina()) {
                tablero[i][j].setMinas(true);
                numeroMinasColocadas++;
                // Logger para saber cuando se ha colocado una mina
                REGISTRO.finest("Se ha generado una mina en las coordenads: [" + i + "," + j + "]");
            } else {
                REGISTRO.finest("Ya hay una mina en las coordenadas: [" + i + "," + j + "]");
            }
        }
        
        // Recuento de minas
        for (int i = 0; i < horizontal; i++) {
            for (int j = 0; j < vertical; j++) {
                contarMinasAlredor(i, j);
            }
        }
        
        //Muestra la colocacion de la minas
        REGISTRO.fine(toString());
    }
    
    /**
     * Calcula las minas que tiene una celda alrededor
     * @param i
     * @param j 
     */
    private void contarMinasAlredor(int i, int j) {
        // Recorrer las celdas que rodean
        byte minasAlrededor = 0;
        for (int k = i - 1; k <= i + 1; k++) {
            for (int l = j - 1; l <= j + 1; l++) {
                if (k >= 0 && k < horizontal && l >= 0 && l < vertical) {
                    if (tablero[k][l].getMina()) {
                        minasAlrededor++;
                    }
                }
            }
        }
        tablero[i][j].setAlrededor(minasAlrededor);
        REGISTRO.finest("La celda (" + i + "," + j + ") tiene " + minasAlrededor + " minas alrededor.");
    }
    
    //Metodo toString para mostrar el contenido de la matriz
    @Override //Sbreescribir el toString que ya traía por defecto de la clase object
    public String toString () {
        String celda = "Minas\n";
        // Pinta la posición de las minas
        for (byte f=0; f < horizontal; f++) {
            for (byte c= 0; c < vertical; c++) {
                if (tablero[f][c].getMina()) {
                    celda += "*";
                } else {
                    celda += "#";
                }
            }
            celda += "\n";
        }
        
        celda += "\nContador de minas\n";
        // Pinta el número de minas que rodean
        for (byte i = 0; i < horizontal; i++) {
            for (byte j = 0; j < vertical; j++) {
                if (tablero[i][j].getMina()) {
                    celda += "*";
                } else {
                    celda += tablero[i][j].getAlrededor();
                }
            }
            celda += "\n";
        }
        
        celda += "\nEstado de celdas\n";
        // Pinta el estado de las celdas
        for (byte i = 0; i < horizontal; i++) {
            for (byte j = 0; j < vertical; j++) {
               switch (tablero[i][j].estadoDeCeldas){
                   case BM_Celda.BANDERITA:
                       celda += "P";
                       break;
                   case BM_Celda.CUBIERTA:
                       celda += "X";
                       break;
                   case BM_Celda.DESTAPADA:
                       celda += "D";
                       break;
               }
            }
            celda += "\n";
        }
        
        // Numero de banderas colocadas
        celda += calcularNumeroCeldasConBanderas() + "\n";
        
        // Final del juego
        celda += detectarFinPartida() ? "GAME OVER\n" : "El juego continúa\n" ;
        
        return celda;
    }
    
    /**
     * Metodo que devuelve si la celda tiene mina o no. Si es 0 hay que 
     * descubrir las celdas que hay alrededor.
     * @param x Coordenada horizontal
     * @param y Coordenada vertical
     * @return True si hay mina 
     */
    public boolean descubrirCelda(int x, int y) {
        BM_Celda unaCelda = tablero[x][y];
        unaCelda.setEstado(BM_Celda.DESTAPADA);
        
        boolean hayMina = unaCelda.getMina();
        if (!hayMina) {
            if (unaCelda.minasAlrededor == 0) {
                for(int k = x - 1; k <= x + 1; k++){
                    for (int l= y-1; l<= y+1; l++){
                         if (k >= 0 && k < horizontal && l >= 0 && l < vertical) {
                             BM_Celda otraCelda = tablero[k][l];
                            if (otraCelda.minasAlrededor == 0 && otraCelda.estadoDeCeldas == BM_Celda.CUBIERTA){
                                descubrirCelda(k, l);                                
                            } else {
                                //Logger de que no vamos a seguir
                            }
                        }
                    }                        
                }
            } else {
                
            }
        }
        return hayMina;
    }
    
    /**
     * Permite colocar en una determinada celda una bandera
     * @param x posicion horizontal de la celda
     * @param y posicion vertical de la celda
     * @return true si se ha colocado la bandera, false si no se coloca
     */
    public boolean colocarBandera(int x, int y) {
        boolean banderaColocada = false;
        BM_Celda unaCelda = tablero[x][y];
        switch (unaCelda.estadoDeCeldas) {
            case BM_Celda.BANDERITA:
                unaCelda.estadoDeCeldas = BM_Celda.CUBIERTA;
                banderaColocada = false;
                REGISTRO.finest("La celda (" + x + "," + y + ") tiene bandera por tanto se elimina la bandera existente y ahora pasa a CUBIERTA.");
                REGISTRO.finest(this.toString());
                break;
            case BM_Celda.CUBIERTA:
                unaCelda.estadoDeCeldas = BM_Celda.BANDERITA;
                banderaColocada = true;
                REGISTRO.finest("La celda (" + x + "," + y + ") está CUBIERTA por tanto se le coloca la bandera.");
                REGISTRO.finest(this.toString());
                break;
            case BM_Celda.DESTAPADA:
                banderaColocada = false;
                REGISTRO.finest("La celda (" + x + "," + y + ") está DESTAPADA por tanto no se coloca bandera.");
                REGISTRO.finest(this.toString());
                break;
        }
        toString();
        return banderaColocada;
    }
    
    /**
     * Calcula el numero de celdas que tienen una bandera
     * @return numero de celdas con bandera
     */
    public int calcularNumeroCeldasConBanderas() {
        int numeroBanderas = 0;
        for (int i = 0; i < horizontal; i++) {
            for (int j = 0; j < vertical; j++) {
                numeroBanderas += tablero[i][j].estadoDeCeldas == BM_Celda.BANDERITA ? 1 : 0;
            }
        }
        return numeroBanderas;
    }
    
    /**
     * Calcula el número de celdas destapadas
     * @return numero de celdas destapadas
     */
    private int calcularNumeroCeldasDestapadas() {
        int numeroBanderas = 0;
        for (int i = 0; i < horizontal; i++) {
            for (int j = 0; j < vertical; j++) {
                numeroBanderas += tablero[i][j].estadoDeCeldas == BM_Celda.DESTAPADA ? 1 : 0;
            }
        }
        return numeroBanderas;
    }
    
    /**
     * Comprueba si la partida a finalizado o no
     * @return true si la partida ha finalizado (numero de celdas descubiertas + numero de minas = numero total de celdas)
     */
    public boolean detectarFinPartida() {
        boolean isGameOver = false;
        
            int numeroTotalCeldas = horizontal * vertical;
            int numeroCeldasDestapadas = calcularNumeroCeldasDestapadas();
            if (numeroCeldasDestapadas + minas == numeroTotalCeldas) {
                isGameOver = true;
        }
            
        return isGameOver;
    }

}
