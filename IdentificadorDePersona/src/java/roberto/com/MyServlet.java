/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roberto.com;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Roberto
 */
@WebServlet(name = "MyServlet", urlPatterns = {"/MyServlet"})
public class MyServlet extends HttpServlet {
    
    public static final byte PETICION_GET = 0;    // SELECT
    public static final byte PETICION_POST = 1;   // INSERT
    public static final byte PETICION_PUT = 2;    // UPDATE
    public static final byte PETICION_DELETE = 3; // DELETE
 
    private byte tipoPeticion;
    private ListaDePersonas personas;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        personas = new ListaDePersonas();
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try  {
            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet NewServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Identificador de personas</h1>");
            
            // Conecta con la base de datos
            EntityManager entityManager = Persistence.createEntityManagerFactory("IdentificadorDePersonaPU")
                        .createEntityManager();

            // Establece la consulta a realizar 
            Query query = entityManager.createNamedQuery("Persona.findAll");

            //Cogemos de la base de datos las personas y las metemos en la lista del objeto persona
            personas.setListaDePersonas(query.getResultList());

            // Iterate the list getting every object
//            for (Persona persona : personas) {
//                out.println("<p>" + persona.getNombre()+ "," + persona.getApellidos()+ "(" + 
//                    persona.getSexo() + ")</p>");
//            }

            // Close connection
            entityManager.close();
            
            JAXBContext jaxbContext = JAXBContext.newInstance(ListaDePersonas.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            
            switch (tipoPeticion) {
                case PETICION_GET:
                    jaxbMarshaller.marshal(personas, out);
//                    for(Persona persona : personas.getListaDePersonas()) {
//                        // Se muestra, por ejemplo, el id y la cadena de caracteres
//                        out.println(persona.getId() + ": " + persona.getNombre() + "<br>");
//                    }    
                    break;
                case PETICION_POST:
                    // Obtener la lista de items que se quieren insertar
                    ListaDePersonas newItems = (ListaDePersonas) jaxbUnmarshaller.unmarshal(request.getInputStream());
                    // Recorrer la lista obteniendo cada objeto contenido en ella
                    for(Persona item :  newItems.getListaDePersonas()) {
                        // Añadir cada objeto a la lista general
                        personas.getListaDePersonas().add(item);
                    }
                    break;
                case PETICION_PUT:
                    // Escribir aquí las accciones para peticiones por PUT
                    break;
                case PETICION_DELETE:
                    // Escribir aquí las accciones para peticiones por DELETE
                    break;
                default:
                    break;
            }    
            
//            out.println("</body>");
//            out.println("</html>");

        } catch (JAXBException ex) {
            Logger.getLogger(Persona.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_GET;
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_POST;
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        tipoPeticion = PETICION_DELETE;
        processRequest(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        tipoPeticion = PETICION_PUT;
        processRequest(req, resp); //To change body of generated methods, choose Tools | Templates.
    }
    

}
